from turtle import Turtle
ALIGNMENT = "center"
FONT = ("Times New Roman", 10, "normal")


class Answer(Turtle):
    def __init__(self):
        super().__init__()
        self.hideturtle()
        self.up()
        # self.writeAnswer(state,position)

    def writeAnswer(self, state, position):
        self.goto(position)
        self.write(f"{state}", move=False,
                   align=ALIGNMENT, font=FONT)

    def gameOver(self):
        self.color("green")
        self.write("You have guessed all states! Congratulations",
                   align=ALIGNMENT, font=("Times New Roman", 20, "bold"))
