import turtle
import pandas
from answers import Answer


data = pandas.read_csv("50_states.csv")
states_list = data.state.to_list()

screen = turtle.Screen()
screen.title("U.S States Game")
image = "blank_states_img.gif"
screen.addshape(image)
turtle.shape(image)

state_count = 0
correct_guesses = []
while state_count < 50:
    if state_count == 0:
        state_input = screen.textinput(
            "Guess the state", "Enter state name").title()
    else:
        state_input = screen.textinput(
            f"{state_count}/50 Correct", "Enter state name").title()
    
    if state_input == "Exit":
        states_to_learn = [item for item in states_list if item not in correct_guesses]

        new_csv = pandas.DataFrame(states_to_learn).to_csv("states_to_learn.csv")
        break

    if state_input not in correct_guesses:
        if not data[data.state == state_input].empty:
            state = data[data.state == state_input]
            state_position = (int(state.x), int(state.y))
            answer = Answer()
            answer.writeAnswer(state.state.item(), state_position)
            state_count += 1
            correct_guesses.append(state.state.item())

    #print(correct_guesses)

if state_count == 50:
    answer.gameOver()

screen.exitonclick()
